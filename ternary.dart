import 'dart:io';

main() {
  print("du liebst mich?");
  stdout.write("jawab (y/t): ");
  String jawab = stdin.readLineSync()!;

  // ternary operator untuk pengganti if/else
  var hasil = (jawab == 'y') ? "menikah" : "jomblo lagi";

  print("Selamat kamu $hasil");
}
