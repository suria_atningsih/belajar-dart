import 'dart:io';

main() {
  // membuat list dengan panjang 5
  var languages = ["Java", "C++", "Javascript", "PHP", "Dart"];

  print("Sebutkan ${languages.length} yang ingin kamu pelajari!");

  for (var i = 1; i < languages.length; i++) {
    stdout.write("$i. ");
    languages[i] = stdin.readLineSync()!;
  }

  print(languages);
}
