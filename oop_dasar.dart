class Person {
  // Person(this._name);
  var _name;
  var _address;

  // fungsi yang mengembalikan nilai
  String getName() {
    return this._name;
  }

  String? setName(String name) {
    this._name = name;
  }

  String getAddress() {
    return this._address;
  }

  void setAddress(String address) {
    this._address = address;
  }
}

// fungsi main
main() {
  var ningsih = new Person();
  ningsih.setName("Ningsih");
  ningsih.setAddress("Mampang");

  print("Nama: ${ningsih.getName()}");
  print("Alamat: ${ningsih.getAddress()}");
}

