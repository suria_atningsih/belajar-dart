void main() {
  // membuat variable dengan tipe data
  String nama = "Ningsih";
  int umur = 24;
  double tinggi = 155.5;
  bool isMenikah = false;

  // membuat variable dengan kata kunci var
  var alamat = "Jakarta, Indonesia";

  // mencetak variable
  print("Nama saya $nama. Umur $umur tahun. Tinggi sekitar $tinggi cm.");
  print("Menikah: $isMenikah");
  print("Alamat: $alamat");
}
