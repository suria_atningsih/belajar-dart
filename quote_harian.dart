import 'dart:io';

main() {
  print("~~~ Quote Harian ~~~");
  stdout.write("Input hari: ");
  String hari = stdin.readLineSync()!.trim().toLowerCase();

  String quote;

  switch (hari) {
    case "senin":
      {
        quote = "Mari kita mulai dari senin";
        break;
      }
    case "selasa":
      {
        quote = "Selesaikan tugas dan bersantailah";
        break;
      }
    case "rabu":
      {
        quote = "Serbu! harii ini penuh semangat";
        break;
      }
    default:
      {
        quote = "Hari yang anda masukan salah";
      }
  }

  print(quote);
}
