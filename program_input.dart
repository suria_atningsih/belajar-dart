import 'dart:io';

main() {
  // stdout.write() ada di library dart:io, sehingga lib itu harus diimport
  // terlebih dahulu
  // write() akan menampilkan teks ke console tanpa membuat baris baru
  stdout.write("Siapa kamu: ");

  // readLineSync() akan membaca input dari keyboard dan return datanya String
  var nama = stdin.readLineSync();
  print("Hello $nama!");
}
